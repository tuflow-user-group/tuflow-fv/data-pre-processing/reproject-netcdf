#Extracts data from TUFLOW FV NetCDF output file to csv for use in an initial conditions csv file for subsequent TUFLOW FV simulations.
#Written by DRK, 11th May 2021.

import os
import xarray as xr
import pandas as pd
from tkinter import Tk
from tkinter.filedialog import askopenfilename

#Tkinter to Select Netcdf file
root = Tk()
File_Name=askopenfilename(filetypes=[("NetCDF", "*.nc")], title="Select NetCDF file to be processed")
root.withdraw()
name = os.path.splitext(File_Name)[0]  # splits file name

# Open netCDF file
nc = xr.open_dataset(File_Name)

#Set the last timestep variable.
last_timestep=nc.dims['Time']-1

#Set the number of 2D and 3D cells variable for checking
num2DCells=nc.dims['NumCells2D']
num3DCells=nc.dims['NumCells3D']

#Can be used to work out what variables are in the netcdf
#variables=nc.variables
#print(variables)

#Read Individual parameters into Individual Dataframes.  Need to Select parameters of interest. TODO: Set this up dynamically
df1 = nc.V_x.to_dataframe()
df2 = nc.V_y.to_dataframe()
df3 = nc.SAL.to_dataframe()
df4 = nc.TEMP.to_dataframe()

#Merge Dataframes.  Sets Num3DCells Multiindex to Column, add 1 to fit in with TUFLOW FV Format
result = pd.concat([df1, df2, df3, df4], axis=1).reindex(df1.index).reset_index(level=['NumCells3D'])
result['NumCells3D'] = result['NumCells3D']+1

#Select Last Timestep.
result=result.iloc[result.index.get_level_values('Time') == last_timestep]

#For Troubleshooting
#print(result)

#Export to CSV
result.to_csv(name+' Combined.csv', index=False) # Remove Time multiindex column so that resulting CSV is in TUFLOW FV Initial Conditions Format
print('Data has been exported, check there are', num3DCells,'3D Cells or',num2DCells, '2D cells')